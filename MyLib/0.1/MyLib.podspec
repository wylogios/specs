Pod::Spec.new do |spec|
  spec.name         = 'MyLib'
  spec.version      = '0.1'
  spec.license      = 'MIT'
  spec.summary      = 'An Objective-C client for the Pusher.com service'
  spec.homepage     = 'https://google.com'
  spec.author       = 'naz'
  spec.source       = { :git => "https://bitbucket.org/wylogios/mylib.git", :tag => "0.1" }
  spec.source_files = 'MyLib/Classes/**/*.{h,m}'
  spec.dependency 'AFNetworking'
  spec.requires_arc = true
end